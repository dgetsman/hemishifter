# Half-life Timer

## Welcome to my project for people suffering from PTSD

I can't say the name of the therapy that it emulates; the therapy method is copyrighted, as is the name, and I'm certainly not looking to break any profiteering laws.  I _can_ tell you, though, that it's the most successful therapy type out there for PTSD at this time.

### Release status

It's only an alpha release; it would be beta, but I'm having problems getting the audio component to it working properly.  Soon as that's working it'll be even better for desensitization of those triggering PTSD memories, and ideally help make it possible for people suffering from PTSD to encounter new stimuli which remind them of those old traumas without having to deal with rage or paralysis, among other symptoms.  As it is, it is still functional; many clinics use similar therapy without the audio stimulation added.

**Stay tuned for further documentation...**

#### Disclaimer

If you're interested, give it a peek.  Please note that I am not a physician or a shrink, and undertaking your own therapy is an absolutely foolish notion that nobody should ever do.  There are probably other disclaimers that I should give for legal reasons but I don't know enough legaleze to spout them just yet.
