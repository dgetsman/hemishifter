module HemiShifter {
	exports hemiShifter;
	requires javafx.graphics;
	requires javafx.controls;
	requires javafx.base;
	requires java.desktop;
	requires com.fasterxml.jackson.databind;
}