package hemiShifter;

import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

public class GlobalMisc {
	public static boolean debugging = false;
	private static Options opts = new Options();
	
	private static Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
	
	/**
	 * Method returns the starting X coordinate for a window with the given X
	 * dimension on the current screen size.
	 * 
	 * @param windowDimension double X dimension of window to place
	 * @return double coordinate to start at on the X axis
	 */
	public static double centerWinderX(double windowDimension) {
		double newCoord = (getPrimaryScreenBounds().getWidth() - windowDimension) / 2;
		debugMsg("centerWinderX", "getKittMaxX: " + opts.getKittMaxX());
		debugMsg("centerWinderX", "windowDimension: " + windowDimension);
		debugMsg("centerWinderX", "newCoord: " + Double.toString(newCoord));
		debugMsg("centerWinderX", "getWidth: " + getPrimaryScreenBounds().getWidth());
		
		return newCoord;
	}
	
	/**
	 * Method returns the starting Y coordinate for a window with the given Y
	 * dimension on the current screen size.
	 * 
	 * @param windowDimension double Y dimension of window to place
	 * @return double coordinate to start at on the Y axis
	 */
	public static double centerWinderY(double windowDimension) {
		double newCoord = (getPrimaryScreenBounds().getHeight() - windowDimension) / 2;
		debugMsg("centerWinderY", "getKittMaxY: " + opts.getKittMaxY());
		debugMsg("centerWinderY", "windowDimension: " + windowDimension);
		debugMsg("centerWinderY", "newCoord: " + Double.toString(newCoord));
		
		return newCoord;
	}
	
	/**
	 * Prints a debugging message to the console, with the prefix of the method
	 * and message to follow.
	 * 
	 * @param method method that call is taking place from
	 * @param message message to print for debugging from the previous method
	 */
	public static void debugMsg(String method, String message) {
		if (debugging) {
			System.out.println(method + ": " + message);
		}
	}
	
	/**
	 * Returns the primaryScreenBounds value if needed somewhere else in the
	 * project.
	 * 
	 * @return Rectangle2D
	 */
	public static Rectangle2D getPrimaryScreenBounds() {
		return primaryScreenBounds;
	}
}
