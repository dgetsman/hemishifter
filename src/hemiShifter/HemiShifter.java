package hemiShifter;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Button;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Class handles entry into the program as well as initialization of resources.
 * 
 * @author sprite
 *
 */
public class HemiShifter extends Application {
	private Options opts = new Options();
	
	private static Region veil = new Region();
	private static GridPane startOrOptionsPane = new GridPane();
	
	private static boolean startOrOptionsFunctional = false;
	
	/**
	 * Initialization for JavaFX, and the 'true' start of our program.  Handles
	 * setting up an initial window for going to the start of the program or 
	 * into the options menu.
	 * 
	 */
	@Override
	public void start(Stage arg0) throws Exception {
		Button startKitt = new Button("Start Session");
		Button viewOptions = new Button("View/Change Options");

		//pane with the buttons initialization
		startKitt.setStyle("-fx-font-weight: bold;");
		viewOptions.setStyle("-fx-font-weight: bold;");
		startKitt.setOnAction(new StartSession());
		viewOptions.setOnAction(new GoUserPrefs());
		
		startOrOptionsPane.setVgap(5); startOrOptionsPane.setHgap(3);
		
		startOrOptionsPane.add(startKitt, 0, 0);
		startOrOptionsPane.add(viewOptions, 1, 0);
		
		Scene scene = new Scene(startOrOptionsPane);
		
		//misc initialization
		veil.setStyle("-fx-background-color: rgba(0, 0, 0, 0.3)");
		veil.setVisible(false);
		
		arg0.setScene(scene);
		arg0.setTitle("HemiShifter");
		
		arg0.show();
		
		arg0.setX(GlobalMisc.centerWinderX(arg0.getWidth()));
		GlobalMisc.debugMsg("start", "getWidth: " + arg0.getWidth());
		//arg0.setY(GlobalMisc.centerWinderY(arg0.getHeight()));
		arg0.setY(opts.getKittMaxY() / 5);
		GlobalMisc.debugMsg("start", "opts.getKittMaxY: " + opts.getKittMaxY());
	}

	/**
	 * Program entry point.  Calls launch() to do the dirty work via javafx's
	 * start() method.
	 * 
	 * @param args normal command line argument array
	 */
	public static void main(String[] args) {
		//initialize that which could not be handled in Options
		//ie handle populating the colors hashmap
		Options.FgColors.put("Dark Red", Color.DARKRED);
		Options.FgColors.put("Red", Color.RED);
		Options.FgColors.put("Orange", Color.ORANGE);
		Options.FgColors.put("Yellow", Color.YELLOW);
		Options.FgColors.put("Green", Color.GREEN);
		Options.FgColors.put("Blue", Color.BLUE);
		Options.FgColors.put("Indigo", Color.INDIGO);
		Options.FgColors.put("Violet", Color.VIOLET);
		
		launch(args);
	}
	
	/**
	 * Method simply toggles whether or not the 'startOrOptionsPane' is
	 * currently disabled or not.
	 * 
	 */
	public static void toggleStartOrOptionsPaneFunctionality() {
		boolean debuggingFunctionality = false;
		
		if (debuggingFunctionality) {
			System.out.println("Toggling functionality of start menu; started with flag: " + startOrOptionsFunctional);
		}
		startOrOptionsFunctional = !startOrOptionsFunctional;
		if (debuggingFunctionality) {
			System.out.println("Functionality is now toggled to: " + startOrOptionsFunctional);
		}
		
		if (startOrOptionsFunctional) {
			//block input from the previous form and go with Kitt
			startOrOptionsPane.setDisable(true);
			veil.setVisible(true);
		} else {
			//resume functionality of our initial pane so that we can do multiple rounds if necessary
			veil.setVisible(false);
			startOrOptionsPane.setDisable(false);
		}
	}
	
	/**
	 * EventHandler for the startKitt button in start(), above.  Handles toggling the original window's usability and instantiates
	 * a new instance of Kitt (the light bar) for usage.
	 * 
	 * @author sprite
	 *
	 */
	private class StartSession implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent arg0) {
			//block input from the previous form and go with Kitt
			//toggleStartOrOptionsPaneFunctionality();
			
			Kitt kitt = new Kitt();
			kitt.go();
			
			//toggleStartOrOptionsPaneFunctionality();
		}
	}
	
	/**
	 * EventHandler for the View Options button in start(), above.  Toggles the original window's usability and instantiates an
	 * instance of the UserSettings menu so that the user can play with the settings in Options.
	 * 
	 * @author sprite
	 *
	 */
	private class GoUserPrefs implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent arg0) {
			//block input from the previous form and go with our settings panel
			//toggleStartOrOptionsPaneFunctionality();
			
			UserSettings userSettingsPanel = new UserSettings();
			//userSettingsPanel.handle(arg0);	//not sure WHY I was doing things this stupidly;
												//maybe not enough caffeine & sleep deprivation?
			userSettingsPanel.init(arg0);
			
			//toggleStartOrOptionsPaneFunctionality();
		}
	}
}
