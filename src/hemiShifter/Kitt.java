package hemiShifter;

import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.time.Instant;

/**
 * Class handles all aspects of creating and animating the Kitt/light bar display for hemisphere switching purposes.
 * 
 * @author sprite
 *
 */
public class Kitt {
	private boolean debugging = false;
	private MyBoolean improperClose = new MyBoolean(true);
	
	//options
	private Options opts = new Options();
	
	//display schitt - Kitt display
	private Stage world = new Stage();
	private Group root = new Group();
	private Scene scene = new Scene(root);
	
	private Canvas ouahPad = new Canvas(opts.getKittMaxX(), opts.getKittMaxY());
	private GraphicsContext gc = ouahPad.getGraphicsContext2D();
	//display schitt - confirmation dialog
	Stage confWorld = new Stage();
	//Group confRoot = new Group();
	StackPane confRoot = new StackPane();
	Scene confScene = new Scene(confRoot);
	
	//audio schitt
	private AudioStim leftAudioStim;
	private AudioStim rightAudioStim;
	//private AudioStim as = new AudioStim();
	
	//other schitt
	private boolean running = true;
	/*private boolean audioErrorLeft = false;
	private boolean audioErrorRight = false;*/
	private int	direction = 1;
	private int location = 1;
	private Instant startInstant;
	
	/**
	 * Method is the entry point for our starting of Kitt's therapy display.
	 * 
	 */
	public void go() {
		//deactivate the initial window for the duration of our therapy
		HemiShifter.toggleStartOrOptionsPaneFunctionality();
		
		init();
		
		root.getChildren().add(ouahPad);
		world.setScene(scene);
		world.setTitle("Therapy Window");
		
		world.setOnHiding(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent arg0) {
				if (improperClose.getValue()) {
					try {
						confWorld.close();
					} catch (Exception ex) {
						GlobalMisc.debugMsg("world.setOnHiding", "confWorld is already closed");
					}
					
					HemiShifter.toggleStartOrOptionsPaneFunctionality();
				}
			}
		});
		
		world.show();
		
		world.setX((GlobalMisc.getPrimaryScreenBounds().getWidth() - world.getWidth()) / 2);
		GlobalMisc.debugMsg("go", "getWidth: " + world.getWidth());
		GlobalMisc.debugMsg("go", "setX: " + ((double)opts.getKittMaxX() - world.getWidth()) / 2);
		world.setY(((GlobalMisc.getPrimaryScreenBounds().getHeight() - world.getHeight()) / 5) * 3);
		GlobalMisc.debugMsg("go", "getHeight: " + world.getHeight());
		GlobalMisc.debugMsg("go", "setY: " + (((double)opts.getKittMaxY() - world.getHeight()) / 5) * 3);
	}
	
	/**
	 * Method handles initialization of the options, fills the original display
	 * window with black, and schedules the timed animation of the display.
	 * 
	 */
	private void init() {		
		//load settings, if possible
		try {
			opts = Options.loadSettings();
			if (debugging) {
				System.out.println("Read " + opts.toString() + " from settings");
			}
		} catch (Exception ex) {
			System.err.println("Error reading settings in Kitt: " + ex.toString() + "\nFalling back to defaults");
		}
		
		//get audio ready
		leftAudioStim = new AudioStim(opts.getAStimFrequency(), 0);
		rightAudioStim = new AudioStim(0, opts.getAStimFrequency());
		
		//set the background the hard way
		//gc.setFill(opts.getBgColor());
		gc.setFill(Color.BLACK);
		gc.fillRect(0, 0, opts.getKittMaxX() - 1, opts.getKittMaxY() - 1);
		
		//start confirmation dialog
		confWorld.setTitle("Start Hemisphere Switching Therapy");
		Button startKitt = new Button("Start Session");
		startKitt.setStyle("-fx-font-weight: bold;");
		startKitt.setOnAction(new StartTherapy());
		confRoot.getChildren().add(startKitt);
		confWorld.setScene(confScene);
		
		confWorld.show();
		
		confWorld.setX(GlobalMisc.centerWinderX(confWorld.getWidth()));
		confWorld.setY((opts.getKittMaxY() / 5) * 3);
	}
	
	/**
	 * Method returns how many eyes there is physical space for on the given display size.
	 * 
	 * @return int number of eyes that can fit in display
	 */
	private int getEyeSpaces() {
		return (int)(opts.getKittMaxX() / 50);
	}
	
	/**
	 * Method increments the location of the eyes in the proper direction 
	 * according to which way everything is going.  It also handles reversing 
	 * direction, if the eyes have reached the bounce point.
	 * 
	 */
	private void incLocation() {
		if (((location == 1) && (direction == -1)) || ((location == (getEyeSpaces() - 1)) && (direction == 1))) {
			location += reverseDirection();
		} else {
			location += direction;
		}
	}
	
	/**
	 * Method handles reversing direction of the eyes' animation.
	 * 
	 * @return
	 */
	private int reverseDirection() {
		direction *= -1;
		
		if (opts.getAStimEnabled()) {
			if (direction == 1) {	//play on the left
				leftAudioStim.play(opts.getAStimDuration());
				leftAudioStim.shutdown();
			} else {				//play on the right
				rightAudioStim.play(opts.getAStimDuration());
				rightAudioStim.shutdown();
			}
		}
		
		return direction;
	}
	
	/**
	 * Method handles drawing the next eye that needs to be put on screen for
	 * the 'traveling eyes' animation.
	 * 
	 */
	private void drawEye() {
		gc.setFill(Options.FgColors.get(opts.getFgColorString()));
		//gc.setFill(Color.DARKRED);
		gc.fillRect((location * 50), 50, opts.getBoxMaxX(), opts.getBoxMaxY());
	}
	
	/**
	 * Method handles removing the eye that needs to be taken away for proper 
	 * traveling of the eyes animation.
	 * 
	 */
	private void eraseEye() {
		int curLoc;
		
		if ((direction == 1) && (location >= opts.getLitEyes())) {	//going right, start wiping
			curLoc = location - (opts.getLitEyes() - 1);
		} else if ((direction == -1) && (location <= (getEyeSpaces() - (opts.getLitEyes() - 1)))) {	//left
			curLoc = location + (opts.getLitEyes() - 1);
		} else {
			return;
		}
		
		//gc.setFill(opts.getBgColor());
		gc.setFill(Color.BLACK);
		gc.fillRect((curLoc * 50), 50, opts.getBoxMaxX(), opts.getBoxMaxY());
	}
	
	/**
	 * Class handles extending the TimerTask to animate the traveling eyes, 
	 * then scheduling the next bounce for whatever increment is in our 
	 * current 'opts' object.
	 * 
	 * @author sprite
	 *
	 */
	//private class BounceTask extends TimerTask {
		/**
		 * Method is the nitty gritty of the BounceTask, containing calls to 
		 * eraseEye, incLocation, and drawEye, then handling the scheduling of 
		 * the following bounce.
		 * 
		 */
		/*public void run() {
			eraseEye();
			incLocation();		
			drawEye();
			
			//keep scheduling new bounces until opts.getSessionDuration() minutes are up, unless debugging, in which case keep
			//scheduling new bounces until 1 second is up
			if ((!debugging && 
					(Instant.now().toEpochMilli() - startInstant.toEpochMilli()) < opts.getSessionDuration() * 60 * 1000) ||
					(debugging && (Instant.now().toEpochMilli() - startInstant.toEpochMilli()) < 1000)) {
				scheduleBounce();
			} else {
				//world.close();
				
				HemiShifter.toggleStartOrOptionsPaneFunctionality();
			}
		}
	}*/
	
	/**
	 * Method handles scheduling the next bounce via our 'timer' object.
	 * 
	 */
	/*private void scheduleBounce() {
		timer.schedule(new BounceTask(), opts.getDefaultPause());
	}*/
	
	/**
	 * Class implements EventHandler to listen for the initial 'start therapy'
	 * dialog box's 'start' click, at which point it starts up the bounce loop
	 * and therapy begins.
	 * 
	 * @author sprite
	 *
	 */
	private class StartTherapy implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent arg0) {
			confWorld.close();
			
			if (running) {
				startInstant = Instant.now();
				
				Task<Boolean> therapyTask = new Task<Boolean>() {
					@Override
					protected Boolean call() throws Exception {
						//keep scheduling new bounces until opts.getSessionDuration() minutes are up, unless debugging, in which case keep
						//scheduling new bounces until 1 second is up
						while ((!debugging && 
								(Instant.now().toEpochMilli() - startInstant.toEpochMilli()) < opts.getSessionDuration() * 60 * 1000) ||
								(debugging && (Instant.now().toEpochMilli() - startInstant.toEpochMilli()) < 1000)) {
							//do the animation
							eraseEye();
							incLocation();
							drawEye();
							
							Thread.sleep(opts.getDefaultPause());
						}
						
						Thread.sleep(1200);	//just so we don't startle them closing it too prematurely
						
						HemiShifter.toggleStartOrOptionsPaneFunctionality();
						
						return true;
					}
					
					@Override
					protected void succeeded() {
						super.succeeded();
						
						improperClose.setValue(false);
						
						world.close();
					}
				};
				
				new Thread(therapyTask).start();
			}
		}
	}
}
