package hemiShifter;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import javafx.scene.paint.Color;

/**
 * Class holds the user modifiable options for working with Kitt and the 
 * methods for saving and loading this data to & from serialized JSON text 
 * on the disk.
 * 
 * @author sprite
 *
 */
public class Options {
	//display options
	private int KittMaxX = 1400;
	private int KittMaxY = 200;
	private int	BoxMaxX = 25;
	private int BoxMaxY = 25;
	private int	LitEyes = 4;
	
	//interval options
	private int DefaultPause = 25;
	private int SessionDuration = 1;	//session duration in minutes
	
	//color prefs
	public static HashMap<String, Color> FgColors = new HashMap<>();
	private String FgColorString = "Dark Red";
	
	//audio stim options
	private boolean AStimEnabled = false;
	private int AStimDuration = 1500;
	private int AStimFrequency = 432;
	private int ASampleRate = 16 * 1024;
	
	//other options
	private final static String settingsPath = new String(".hemiShifter.json");
	
	//constructors
	public Options() { }
	
	//getters/setters
	public int getKittMaxX() {
		return KittMaxX;
	}
	public void setKittMaxX(int kittMaxX) {
		KittMaxX = kittMaxX;
	}
	
	public int getKittMaxY() {
		return KittMaxY;
	}
	public void setKittMaxY(int kittMaxY) {
		KittMaxY = kittMaxY;
	}
	
	public int getBoxMaxX() {
		return BoxMaxX;
	}
	public void setBoxMaxX(int boxMaxX) {
		BoxMaxX = boxMaxX;
	}
	
	public int getBoxMaxY() {
		return BoxMaxY;
	}
	public void setBoxMaxY(int boxMaxY) {
		BoxMaxY = boxMaxY;
	}
	
	public int getLitEyes() {
		return LitEyes;
	}
	public void setLitEyes(int litEyes) { /*else {
		//world.close();
		
		HemiShifter.toggleStartOrOptionsPaneFunctionality();
	}*/
		LitEyes = litEyes;
	}
	
	/*public Color getBgColor() {
		return bgColor;
	}
	public void setBgColor(Color bgColor) {
		this.bgColor = bgColor;
	}
	
	public Color getFgColor() {
		return fgColor;
	}
	public void setFgColor(Color fgColor) {
		this.fgColor = fgColor;
	}*/
	
	public int getDefaultPause() {
		return DefaultPause;
	}
	public void setDefaultPause(int defaultPause) {
		this.DefaultPause = defaultPause;
	}
	
	public int getSessionDuration() {
		return SessionDuration;
	}
	public void setSessionDuration(int sessionDuration) {
		this.SessionDuration = sessionDuration;
	}
	
	public int getAStimDuration() {
		return AStimDuration;
	}
	public void setAStimDuration(int aStimDuration) {
		AStimDuration = aStimDuration;
	}

	public int getAStimFrequency() {
		return AStimFrequency;
	}
	public void setAStimFrequency(int aStimFrequency) {
		AStimFrequency = aStimFrequency;
	}

	public int getASampleRate() {
		return ASampleRate;
	}
	public void setASampleRate(int aSampleRate) {
		ASampleRate = aSampleRate;
	}
	
	public boolean getAStimEnabled() {
		return AStimEnabled;
	}
	public void setAStimEnabled(boolean aStimEnabled) {
		AStimEnabled = aStimEnabled;
	}
	
	public String getFgColorString() {
		return FgColorString;
	}
	public void setFgColorString(String colorString) {
		FgColorString = colorString;
	}
	
	/**
	 * Handles serializing and saving of the [presumably modified] Options 
	 * fields via the jackson serialization library.
	 * 
	 * @throws Exception issues with serialization
	 */
	public void saveSettings() throws Exception {
		BufferedOutputStream bosSettings = new BufferedOutputStream(new FileOutputStream(settingsPath));
		ObjectMapper om = new ObjectMapper();
		
		try {
			om.writeValue(bosSettings, this);
		} catch (Exception ex) {
			System.err.println("Serialization exception: " + ex.toString());
		}
		
		bosSettings.close();
	}
	
	/**
	 * Handles loading and deserialization of whatever Options fields may have
	 * been saved to disk via the jackson serialization library.
	 * 
	 * @return new Options object
	 * @throws Exception issues with loading/deserialization of JSON on the 
	 * disk
	 */
	public static Options loadSettings() throws Exception {
		ObjectMapper om = new ObjectMapper();
		File fSettings = new File(settingsPath);
		String fPath = fSettings.getPath();
		Path pSettings = Paths.get(settingsPath);
		
		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		if (fSettings.exists() && Files.isReadable(pSettings)) {
			return om.readValue(fSettings, Options.class);
		}
		
		throw new Exception("File " + settingsPath + " doesn't exist or is not readable!");
	}
	
	/**
	 * See our data as a string.
	 * 
	 */
	public String toString() {
		return "Options [KittMaxX: " + KittMaxX + ", KittMaxY: " + KittMaxY + ", BoxMaxX: " + BoxMaxX + ", BoxMaxY: " + 
				BoxMaxY + ", LitEyes: " + LitEyes + ", DefaultPause: " + DefaultPause + ", FgColorString: " + FgColorString + "]"; 
	}
}
