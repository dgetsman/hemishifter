package hemiShifter;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.Scene;

/**
 * Class handles setting up a window for the user to modify and save the program's settings.
 * 
 * @author sprite
 *
 */
public class UserSettings implements EventHandler<ActionEvent> {
	private boolean debugging = false;
	//private Boolean improperClose = new Boolean(true);
	private MyBoolean improperClose = new MyBoolean(true);
	
	//configuration panel setup
	private Stage userSetStage = new Stage();
	private GridPane userSettingsGrid = new GridPane();
	Button saveExit = new Button("Save & Exit");
	Button abandonExit = new Button("Abandon & Exit");
	Label lblSetting = new Label("Setting");
	Label lblValue = new Label("Value");
	
	private Options opts = new Options();
	
	/**
	 * Method handles initialization of the UserSettings UI.
	 * 
	 * @param arg0
	 */
	public void init(ActionEvent arg0) {
		//first hide the initial window and deactivate it for our bit working
		//with options
		HemiShifter.toggleStartOrOptionsPaneFunctionality();
		
		//a little more configuration for the preferences panel
		//gotta make it PWETTY
		saveExit.setStyle("-fx-font-weight: bold;");
		abandonExit.setStyle("-fx-font-weight: bold;");
		lblSetting.setStyle("-fx-font-weight: bold;");
		lblValue.setStyle("-fx-font-weight: bold;");
		
		userSettingsGrid.setVgap(5); userSettingsGrid.setHgap(3);
		
		//settings description column
		userSettingsGrid.add(lblSetting, 0, 0);
		userSettingsGrid.add(lblValue, 1, 0);
		
		/*
		 * here are the settings that we need to be able to work with here:
		 * int KittMaxX = 1400;
		 * int KittMaxY = 200;
		 * int	BoxMaxX = 25;
		 * int BoxMaxY = 25;
		 * int	LitEyes = 4;
		 * int DefaultPause = 25;
		 * Color bgColor = Color.BLACK;
		 * Color fgColor = Color.DARKRED;
		 */
		userSettingsGrid.add(new Label("Display Length"), 0, 1);
		userSettingsGrid.add(new TextField(Integer.toString(opts.getKittMaxX())), 1, 1);
		userSettingsGrid.add(new Label("Display Height"), 0, 2);
		userSettingsGrid.add(new TextField(Integer.toString(opts.getKittMaxY())), 1, 2);
		userSettingsGrid.add(new Label("Eye Length"), 0, 3);
		userSettingsGrid.add(new TextField(Integer.toString(opts.getBoxMaxX())), 1, 3);
		userSettingsGrid.add(new Label("Eye Height"), 0, 4);
		userSettingsGrid.add(new TextField(Integer.toString(opts.getBoxMaxY())), 1, 4);
		userSettingsGrid.add(new Label("Number of Eyes"), 0, 5);
		userSettingsGrid.add(new TextField(Integer.toString(opts.getLitEyes())), 1, 5);
		userSettingsGrid.add(new Label("Milliseconds Between Updates"), 0, 6);
		userSettingsGrid.add(new TextField(Integer.toString(opts.getDefaultPause())), 1, 6);
		userSettingsGrid.add(new Label("Session Duration in Minutes"), 0, 7);
		userSettingsGrid.add(new TextField(Integer.toString(opts.getSessionDuration())), 1, 7);
		//skipping work with the colors right now because I don't want to mess with learning a color selector yet
		userSettingsGrid.add(new Label("Kitt Foreground Color"), 0, 8);
		ComboBox<String> fgColorSelector = new ComboBox<>();
		fgColorSelector.getItems().addAll(Options.FgColors.keySet());
		fgColorSelector.setValue(opts.getFgColorString());
		userSettingsGrid.add(fgColorSelector, 1, 8);
		
		userSettingsGrid.add(saveExit, 0, 10);
		userSettingsGrid.add(abandonExit, 1, 10);
		
		Scene userSetScene = new Scene(userSettingsGrid);
		userSetStage.setScene(userSetScene);
		userSetStage.setTitle("Ya just couldn't leave good enough alone...");
		
		userSetStage.setOnHiding(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent arg0) {
				if (improperClose.getValue()) {
					HemiShifter.toggleStartOrOptionsPaneFunctionality();
				}
			}
		});
		
		try {
			opts = Options.loadSettings();
		} catch (Exception ex) {
			System.err.println("Unable to load settings in UserSettings: " + ex.toString() + "\nFalling back to defaults");
		}
		
		//register handlers
		handle(arg0);
		
		userSetStage.show();
	}
	
	/**
	 * Handler for clicking save 'n exit or abandon changes.
	 * 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void handle(ActionEvent arg0) {
		saveExit.setOnAction(new EventHandler() {
			/**
			 * Handles a click of the 'saveExit' button; parses the proper form fields and saves everything via
			 * Options.saveSettings().
			 * 
			 */
			@Override
			public void handle(Event arg0) {
				//KittMaxX
				opts.setKittMaxX(Integer.parseInt(((TextField) userSettingsGrid.getChildren().get(3)).getText()));
				//KittMaxY
				opts.setKittMaxY(Integer.parseInt(((TextField) userSettingsGrid.getChildren().get(5)).getText()));
				//EyeMaxX
				opts.setBoxMaxX(Integer.parseInt(((TextField) userSettingsGrid.getChildren().get(7)).getText()));
				//EyeMaxY
				opts.setBoxMaxY(Integer.parseInt(((TextField) userSettingsGrid.getChildren().get(9)).getText()));
				//number of eyes
				opts.setLitEyes(Integer.parseInt(((TextField) userSettingsGrid.getChildren().get(11)).getText()));
				//pause between display updates
				opts.setDefaultPause(Integer.parseInt(((TextField) userSettingsGrid.getChildren().get(13)).getText()));
				//total session duration
				opts.setSessionDuration(Integer.parseInt(((TextField) userSettingsGrid.getChildren().get(15)).getText()));
				//default foreground color for Kitt
				//opts.setFgColor(Options.FgColors.get(((ComboBox) userSettingsGrid.getChildren().get(15)).getValue()));
				opts.setFgColorString(((ComboBox) userSettingsGrid.getChildren().get(17)).getValue().toString());
				if (debugging) {
					System.out.println("Setting FgColor to value in HashMap for: " + 
							((ComboBox) userSettingsGrid.getChildren().get(17)).getValue());
				}
				
				try {
					opts.saveSettings();
				} catch (Exception ex) {
					System.err.println("Exception in Options.saveSettings(): " + ex.toString());
				}
				
				//HemiShifter.toggleStartOrOptionsPaneFunctionality();
				userSetStage.close();
			}
		});
		abandonExit.setOnAction(new EventHandler() {
			/**
			 * Handles closing the form if the user doesn't want to save the [presumably modified] settings.
			 * 
			 */
			@Override
			public void handle(Event arg0) {
				improperClose.setValue(false);
				
				HemiShifter.toggleStartOrOptionsPaneFunctionality();
				
				userSetStage.close();
			}
		});
	}
}
