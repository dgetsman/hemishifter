package hemiShifter;

public class MyBoolean {
	boolean	myValue;
	
	public MyBoolean(boolean initialValue) {
		this.myValue = initialValue;
	}
	
	public void setValue(boolean value) {
		this.myValue = value;
	}
	
	public boolean getValue() {
		return this.myValue;
	}
}
