package hemiShifter;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

/**
 * Class handles all of the ins and outs of dealing with our audio subroutines 
 * and producing the audio stim at the end of each eyes direction change.
 *  
 * @author sprite
 *
 */
public class AudioStim {
	private Options opts;
	private boolean audioError = false;
	private boolean debugging = false;
	
	private static final int SAMPLE_RATE = 16 * 1024; // 16KHz
    private static final int SAMPLE_SIZE = 8; // 8 bits per sample
    private static final int NUM_CHANNELS = 2; // Stereo
	private final byte leftChannel[] = new byte[SAMPLE_RATE];
	private final byte rightChannel[] = new byte[SAMPLE_RATE];
	private SourceDataLine line = null;
	private final AudioFormat audioFormat = new AudioFormat(
			SAMPLE_RATE,
			SAMPLE_SIZE,
			NUM_CHANNELS,
			true,
			true
	);
	
	//put here just to show the original example:
	/*public static void main(String[] args) {
        int leftFreq = 440; // A = 440Hz
        int rightFreq = 448;
        Binaural binauralGenerator = new Binaural(leftFreq, rightFreq);
        binauralGenerator.play(3); // Play for 3 seconds
        binauralGenerator.shutdown();
    }*/
	
	/**
	 * Method produces the audio specified by leftFreq and rightFreq and puts
	 * it out over the line.
	 * 
	 * @param leftFreq
	 * @param rightFreq
	 */
	public AudioStim(int leftFreq, int rightFreq) {
		//get our settings, if customized
		try {
			opts = Options.loadSettings();
		} catch (Exception ex) {
			GlobalMisc.debugMsg("AudioStim", "Unable to load settings in AudioStim, utilizing defaults: " + ex);
			//System.out.println("Unable to load settings in AudioStim, utilizing defaults");
			opts = new Options();
		}
		
		if (debugging) {
			System.out.println("Initializing with left frequency: " + leftFreq + ", right frequency: " + rightFreq);
		}
		
		//fill left & right channel buffers with sine waves
		fillBufferWithSineWave(leftChannel, leftFreq);
		fillBufferWithSineWave(rightChannel, rightFreq);
		
		//initialize audio output
		try {
			line = AudioSystem.getSourceDataLine(audioFormat);
			line.open(audioFormat, (int) audioFormat.getSampleRate());
			line.start();
		} catch (LineUnavailableException ex) {
			if (!audioError) {
				audioError = true;
				System.err.println("Unable to get audio line for output");
			}
		}
	}
	
	/**
	 * It's been so long since I've worked with this project that I don't
	 * really remember the ins and outs very well of audio production all that
	 * well...  Just go by the name of the method at this point, I guess.
	 * 
	 * @param buffer
	 * @param frequency
	 */
	private void fillBufferWithSineWave(byte[] buffer, int frequency) {
		double period = (double) SAMPLE_RATE / frequency;
		for (int cntr = 0; cntr < SAMPLE_RATE; cntr++) {	//fill each byte of the buffer
			double angle = 2.0 * Math.PI * cntr / period;
			buffer[cntr] = (byte) (Math.sin(angle) * 127f);
		}
	}
	
	/**
	 * Apparently this method plays the audio that has been generated.
	 * 
	 * @param milliSeconds
	 */
	public void play(int milliSeconds) {
		int samplePosition = 0;
		int endPosition = (int)audioFormat.getSampleRate();
		
		if (debugging) {
			System.out.println("SAMPLE_RATE: " + SAMPLE_RATE + " milliSeconds: " + milliSeconds);
			System.out.println("sample rate modification: " + (int)(SAMPLE_RATE * (milliSeconds / (double)1000) / (double)2));
		}
		
		//so the SAMPLE_RATE modification is our issue here, as far as the no playback for <1sec is concerned
		for (int cntr = 0; cntr < (int)(SAMPLE_RATE * (milliSeconds / (double)1000) / (double)2); cntr++) {
			line.write(leftChannel,  samplePosition, 2);	//2 bytes from each
			line.write(rightChannel, samplePosition, 2);
			
			samplePosition += 2;
			if (samplePosition >= endPosition) {
				samplePosition = 0;
			}
		}
	}
	
	/**
	 * Method handles shutting down the audio channel.
	 * 
	 */
	public void shutdown() {
		line.flush();
		line.close();
	}
	
	/*private byte[] leftStereoToneBuffer, rightStereoToneBuffer;
	private int bufferLength = 0;
	private Options opts = null;
	private boolean debugging = true;
	
	//constructor
	public AudioStim() {
		try {
			opts = Options.loadSettings();
		} catch (Exception ex) {
			opts = new Options();
			
			System.err.println("Unable to load settings in AudioStim, utilizing defaults");
		}
		
		//assuming stereo audio; we're not even going to mess with mono as that would have no real purpose here anyway
		bufferLength = (int)((opts.getAStimDuration() * opts.getASampleRate()) / 1000);	//the * 2 _was_ due to stereo
		
		leftStereoToneBuffer = new byte[bufferLength * 2];
		rightStereoToneBuffer = new byte[bufferLength * 2];	//were these multiplied by 2 because they were stereo?  do we have
															//some redundancy here that may break things?
		
		createSinWaveBuffer();
	}
	
	private void createSinWaveBuffer() {
		double period = (double)(opts.getASampleRate() / opts.getAStimFrequency());
		
		for (int cntr = 0; cntr < bufferLength; cntr += 2) {
			double angle = 2.0 * Math.PI * (cntr / period);
			short nakk = (short)(Math.asin(angle) * 32767);
			
			rightStereoToneBuffer[cntr] = (byte)(nakk & 0xFF);
			leftStereoToneBuffer[cntr] = (byte)(nakk >> 8);
			rightStereoToneBuffer[cntr + 1] = 0;
			rightStereoToneBuffer[cntr + 1] = 0;
		}
	}
	
	public void playTone(boolean leftHanded) throws LineUnavailableException {	//true for left, false for right-- a poor way, but it works
		final AudioFormat af;
		
		af = new AudioFormat(opts.getASampleRate(), 8, 1, true, true);
		SourceDataLine line = AudioSystem.getSourceDataLine(af);
		
		line.open(af, opts.getASampleRate());
		line.start();
		
		if (leftHanded) {
			line.write(leftStereoToneBuffer, 0, (bufferLength * 2));
		} else {
			line.write(rightStereoToneBuffer, 0, (bufferLength * 2));
		}
		
		line.drain();
		line.close();
	}*/
}
